const car = (name, model, owner, year, phone, image, published) => ({name, model, owner, year, phone, image, published})

const cars = [
    car('Ford', 'Focus', 'TOM', '2015', '+790000000', '1.png', new Date),
    car('Bmw', 'i8', 'SAM', '2018', '+790000000', '2.png',  new Date)
]

new Vue({
    el: '#app',
    data: {
        cars: cars,
        activeCar: cars[0],
        activeCarIndex: 0,
        phoneVis: false,
        search: ''
    },
    methods: {
        selectCar: function(index) {
            this.activeCar = cars[index];
            this.activeCarIndex = index;
        }
    },
    computed: {
        phoneBtnText() {
            return this.phoneVis ? 'Hide phone' : 'Show phone'
        },
        filteredCars() {
            const filtered = this.cars.filter(car => {
                return car.name.indexOf(this.search) > -1 || car.model.indexOf(this.search) > -1
            })
            return filtered;
        }

    },
    filters: {
        date(value) {
            return value.toLocaleString();
        }
    }
}) 